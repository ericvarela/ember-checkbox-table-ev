import Component from '@ember/component';
import { computed } from '@ember/object';
import { A } from '@ember/array';

export default Component.extend({
  tableName: 'default',

  init() {
    this._super(...arguments);
    this.handleResize = function() {
      console.log($(window).height());
      $(".scrollable-table").height($(window).height() - 400);
    };
    Ember.run.next(this, this.handleResize);
    $(window).on('resize', Ember.run.bind(this, this.handleResize));
  },

  allChecked: computed('checkedRows' , function(){
    var rowData = this.get('rowData');
    var allRows = rowData.tableTopRows.concat(rowData.tableBottomRows)
    var checkedRows = this.get('checkedRows');
    return checkedRows.length === allRows.length;
  }),

  allUnchecked: computed('checkedRows' , function(){
    var checkedRows = this.get('checkedRows');
    return !checkedRows.length;
  }),

  updateAllChecked : function(){
    // the action to passed to Component
    this.updateChecked(this.get("checkedRows"));
  },

  actions: {

    syncAllChecked:function(){
      this.updateAllChecked()
    },

    checkAll: function(){
      var rowData = this.get('rowData');
      var allRows = rowData.tableTopRows.concat(rowData.tableBottomRows)
      var newCheckedArray = A([]);
      allRows.forEach(function(item){
        newCheckedArray.push(item.id)
      });
      this.set("checkedRows", newCheckedArray);
      this.updateAllChecked();
    },

    uncheckAll: function(){
      var newCheckedArray = A([]);
      this.set("checkedRows", newCheckedArray);
      this.updateAllChecked();
    }
  }
});
