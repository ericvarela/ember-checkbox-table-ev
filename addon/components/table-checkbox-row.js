import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: 'tr',
  classNameBindings: ['row.checked:checkbox-row-checked:checkbox-row-unchecked', 'hasSeparator:checkbox-row-has-separator'],
  useCommas: false,
  showAll: false,

  isChecked: computed('row.id', 'checkedRows', 'checkedRows.@each' , function(){
    var checkedRows = this.get('checkedRows');
    let checked = checkedRows.includes(this.get("row.id"));
    return checked;
  }),

  actions: {
    clickRow() {
      var checkedRows = this.get('checkedRows');
      let checked = this.get('isChecked');
      let id = this.get("row.id");
      if (!checked){
        checkedRows.addObject(id);
      } else {
        checkedRows.removeObject(id);
      }
      this.updateAllChecked();
    },
    clickBox(event){
      var checkedRows = this.get('checkedRows');
      let checked = this.get('isChecked');
      let id = this.get("row.id");

      if (!checked){
        checkedRows.addObject(id);
      } else {
        checkedRows.removeObject(id);
      }
      this.updateAllChecked();
      event.stopPropagation();
    },
    showRows: function () {
      this.set('showAll', !this.get('showAll'));
      return false;
    },
    nullAction: function(event) {
      event.stopPropagation();
    }
  }
});
